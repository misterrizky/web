<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('hrm.users', function (Blueprint $table) {
            $table->id('id_user');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo');
            $table->enum('roles',['Employee', 'Member','Super Admin'])->default('Member')->nullable();
            $table->string('google_id')->nullable();
            $table->rememberToken();
            $table->timestamp('banned_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('hrm.users');
    }
}
